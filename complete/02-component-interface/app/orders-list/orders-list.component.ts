import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Order } from '../model/order';

@Component({
    selector: 'app-orders-list',
    templateUrl: 'orders-list.component.html'
})
export class OrdersListComponent {

    @Input() orders: Order[];
    @Input() selectedOrder: Order;

    @Output() selectedOrderChange = new EventEmitter<Order>();

    select(order: Order) {
        this.selectedOrderChange.emit(order);
    }
}
