import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Room } from './model/chat';
import { Observable } from 'rxjs';
import { ChatService } from './chat.service';

@Injectable()
export class RoomsResolver implements Resolve<Room[]> {
  constructor(private chatService: ChatService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Room[]> {
    return this.chatService.getRooms();
  }
}
