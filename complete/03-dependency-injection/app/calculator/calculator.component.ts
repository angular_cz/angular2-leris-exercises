import { Component } from '@angular/core';
import { Book } from '../model/book';
import { CalculatorService } from '../calculator.service';
import { SavedCalculationsService } from '../saved-calculations.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html'
})
export class CalculatorComponent {

  book: Book;

  constructor(private calculator: CalculatorService,
              private savedCalculations: SavedCalculationsService) {

    this.clearCalculator();
  }

  private clearCalculator() {
    this.book = {
      title: 'Book title',
      totalPages: 0,
      colorPages: 0
    };
  }

  getPrice() {
    return this.calculator.calculate(this.book);
  }

  saveCalculation() {
    this.savedCalculations.saveCalculation(this.book);
    this.clearCalculator();
  }

  getCalculations() {
    return this.savedCalculations.savedCalculations;
  }
}
